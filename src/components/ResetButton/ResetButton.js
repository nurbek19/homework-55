import React from 'react';

const ResetButton = props => {
  return <button onClick={props.resetGame}>Reset</button>
};

export default ResetButton;