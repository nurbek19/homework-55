import React from 'react';
import './Square.css'

const Square = props => {

    return (
        <div className={props.squareClass.join(' ')} onClick={props.click}>
            <span>{props.ring}</span>
        </div>
    )
};

export default Square;