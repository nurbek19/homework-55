import React from 'react';
import Square from './Square/Square'
import './Area.css'


const Area = props => {
    return props.squares.map((square) => {
        if (!square.hasItem) {
            return <Square
                key={square.id}
                click={() => props.click(square.id)}
                squareClass={square.squareClass}
                ring={null}
            />;
        } else {
            return <Square
                key={square.id}
                click={() => props.click(square.id)}
                squareClass={square.squareClass}
                ring="O"
            />;
        }
    });
};

export default Area;