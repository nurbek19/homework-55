import React from 'react';
import './TryAmount.css';

const TryAmount = props => {
  return <p className="tries-amount">Tries: {props.tryAmount}</p>
};

export default TryAmount;