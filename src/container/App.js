import React, {Component} from 'react';
import Area from '../components/Area/Area';
import TryAmount from '../components/TryAmount/TryAmount';
import ResetButton from '../components/ResetButton/ResetButton'
import './App.css';

class App extends Component {
    state = {
        squares: [],
        tryAmount: 0
    };

    createStateItems() {
        let squares = [];
        const randomIndex = Math.floor(Math.random() * 36);

        for (let i = 0; i < 36; i++) {
            const item = {
                hasItem: false,
                isClicked: false,
                squareClass: ['square-inactive'],
                id: i
            };

            if (i === randomIndex) {
                item.hasItem = true;
            }

            squares.push(item);
        }

        this.setState({squares});
    };

    componentDidMount() {
        this.createStateItems();
    }

    handleClick = (id) => {
        const index = this.state.squares.findIndex(s => s.id === id);
        const square = {...this.state.squares[index]};
        let tryAmount = this.state.tryAmount;

        if (!square.isClicked) {
            square.squareClass.push('square-active');
            square.isClicked = true;
            tryAmount++;
        }

        if (square.hasItem) {
            setTimeout(function () {
                alert('You have found ring!');
            }, 300);
        }

        const squares = [...this.state.squares];
        squares[index] = square;

        this.setState({squares, tryAmount});
    };

    resetGame = () => {
        this.createStateItems();
        this.setState({tryAmount: 0});
    };

    render() {
        return (
            <div className="App">
                <div className="Area">
                    <Area squares={this.state.squares} click={this.handleClick}/>
                </div>

                <TryAmount tryAmount={this.state.tryAmount}/>

                <ResetButton resetGame={this.resetGame}/>
            </div>
        );
    }
}

export default App;
